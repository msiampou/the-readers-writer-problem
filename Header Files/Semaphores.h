//Header file for Semaphores

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>

union semun{
    int val;
    struct semid_ds *buf;
    ushort *array;
};

// Get and initialize semaphores set
int semCreate(key_t,int,int);

//Wait Sem
int semDown(int,int);
//Signal Sem
int semUp(int,int);
// Delete semaphore
int semDel(int);
//Get the value of Sem
int Get(int, int);
//Set the value of Sem
int Set(int, int, int);

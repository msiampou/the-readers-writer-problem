//Header File for Shared Memory Segment

#pragma once

#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

typedef struct data{        //shared memory storages a struct
  int val;
  //int count;
  long double ts;
}MemData;

//Create shared memory and connect
int ShMCreate(key_t);

//Getting a pointer to the segment
MemData *ShMAttach(int);

//Detaching segment
int ShMDetach(MemData*);

//Deleting shared memory segment
int ShMDelete(int);

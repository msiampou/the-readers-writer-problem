#include "SharedMem.h"

int ShMCreate(key_t key){

    if(key<0){
      printf("*** key error ***");
      return -1;
    }

    return shmget(key, sizeof(MemData), IPC_CREAT | 0666);
}

MemData* ShMAttach(int ShmID){
    return shmat(ShmID, (void *)0, 0);
}

int ShMDetach(MemData* ShmPTR){
    return shmdt(ShmPTR);
}

int ShMDelete(int ShmID){
    return shmctl(ShmID, IPC_RMID, 0);
}

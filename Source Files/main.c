//Main Program

#include <sys/wait.h>
#include <unistd.h>
#include <sys/timeb.h>
#include <time.h>
#include <stdlib.h>

#include "SharedMem.h"
#include "Semaphores.h"


int main(int argc, char* argv[]){

    srand(time(NULL));

    struct timespec tfather = {0,0}, tchild = {0,0};

    FILE *f = fopen("test.txt", "w");

    int n, i, m, j, status = 0;
    int *Matrix, *Ch_Array;
    long double dt;

    pid_t pid;
    key_t key = 5468;

    int ShmID;
    MemData *ShmPTR;

    int Writer_ID, Readers_ID, Mutex_ID, Print_ID;

    if(argc < 1){
        printf("Error of argc");
    }
    else if(argc < 3){
        printf("Error of argc");
    }
    else{
        n = atoi(argv[1]);
        m = atoi(argv[2]);
    }

    //create the key
    /*if ((key = ftok("fork.c", 'R')) == -1) {                //producing a random key for shared memory
        printf("*** ftok error ***\n");
            exit(1);
    }*/

    ShmID = ShMCreate(key);                                 //Creating and Attaching shared memory segment
    if (ShmID < 0) {
        printf("*** shmget error (server) ***\n");
        exit(1);
    }
    ShmPTR = ShMAttach(ShmID);
    if (ShmPTR == (MemData *)(-1)){
        printf("*** shmat error (server) ***\n");
        exit(1);
    }

    Writer_ID = semCreate((key_t)1234,1,1);                        //Create a binary semaphore
    if (Writer_ID < 0){
        printf("***semget error***\n");
        exit(0);
    }

    Readers_ID = semCreate((key_t)2345,n,0);                       //Creating a set of n binary semaphores
    if (Readers_ID < 0){
        printf("***semget error1***\n");
        exit(0);
    }

    Mutex_ID = semCreate((key_t)3245,1,0);                         //Creating a counting semaphore
    if (Mutex_ID < 0){
        printf("***semget error***\n");
        exit(0);
    }

    Print_ID = semCreate((key_t)46548,1,1);                         //Creating a binary semaphore
    if (Print_ID < 0){
        printf("***semget error***\n");
        exit(0);
    }

    for(i=0; i<n; i++){                     //Creating n child processes
        pid = fork();
        if(pid==0)          //if child, don't fork()
          break;
    }

    if(pid != 0){                               //We are on father

        Matrix = malloc(m*sizeof(int));        //Creating father's matrix
        if (Matrix==NULL){
        printf("Memory Error");
            return 1;
        }

        printf("Matrix copied into shared memory segment: ");       //Fill the matrix with random numbers
        for(i=0;i<m;i++){
            Matrix[i] = rand()%21;
            printf("%d ", Matrix[i]);
        }
        printf("\n");

        // Copy array cell by cell to shared mem
        for(j=0;j<m;j++){
            semDown(Writer_ID,0);                           //Block Writing

            ShmPTR->val = Matrix[j];                        //Writing on Shared Memory segment
            clock_gettime(CLOCK_MONOTONIC, &tfather);
            ShmPTR->ts = (double)(tfather.tv_nsec);

            Set(Mutex_ID, 0, 0);                    //Set Mutex's Value to 0
            int k;
            for(k=0;k<n;k++)                        //Unblock all reading processes
                semUp(Readers_ID, k);
        }
        wait(&status);
        free(Matrix);
    }
    else{

        Ch_Array = (int*) malloc(m*sizeof(int));           //Creating n Arrays for every child process
        if (Ch_Array==NULL){
            printf("Memory Error");
            return 1;
        }

        for(j=0;j<m;j++){

            semDown(Readers_ID,i);                          //Block i Process from reading the same data twice

            Ch_Array[j] = ShmPTR->val;                      //Reading
            clock_gettime(CLOCK_MONOTONIC, &tchild);
            dt = (double)(tchild.tv_nsec - ShmPTR->ts);      //counting time average

            semUp(Mutex_ID,0);                               //Increase counting semaphore

            if(Get(Mutex_ID,0) == n){                       //If all the processes have read the context
                semUp(Writer_ID,0);                         //Unblock Writer
            }
        }
    }

    if(pid==0){
        semDown(Print_ID,0);
        fprintf(f,"\nI am a Child with pid: %d\n",getpid());
        fprintf(f,"\nMy Running Average in nanoseconds is: %Lf\n", (dt/m));
        fprintf(f,"My Matrix: ");
        for(j=0;j<m;j++)
            fprintf(f,"%d ", Ch_Array[j]);
        fprintf(f,"\n\n");
        semUp(Print_ID,0);
        free(Ch_Array);
        exit(0);                        //child exits
    }

    if (ShMDetach(ShmPTR) == -1) {                  //Detaching and Deleting SharedMem
        printf(" ***shmdt failed***\n");
        exit(1);
    }
    if (ShMDelete(ShmID) == -1) {
        printf(" ***shmctl(IPC_RMID) failed***\n");
        exit(1);
    }

    semDel(Writer_ID);              //Deleting semaphores
    semDel(Readers_ID);
    semDel(Mutex_ID);
    semDel(Print_ID);

    printf("\nCheck test.txt for results\n");
    fclose(f);
    exit(0);                        //parent exits
    fprintf(f,"\n");
    return 0;
}

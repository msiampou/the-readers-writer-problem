objects=main.o Semaphores.o SharedMem.o
ex1 : $(objects)
	gcc $(objects) -o ex1
main.o : Semaphores.h SharedMem.h
Semaphores.o : Semaphores.h
SharedMem.o: SharedMem.h
clean:
	rm ex1 $(objects)
